<?php

//this gona be global variable array of mysql config settings, database config , 
//names for thing like cookies and different names for thing like sessions

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        //database properties
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'db' => 'login_oop'
    ),
    //this is going to be cookie names for remember me on login
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800,
    ),
    //this is going to be seesion names and tokens
    'session' => array(
        'session_name' => 'user'
    ),
    
);
//require_once '../functions/sanitaize.php';
// it is extreamly importatn when data entering in db and exporting data from db

function escape($string){
    //ENT_QUOTES based on single and double quotes
    //third parameter is defining the character decoding codex
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

//we will make autoloader for including files automatic
//spl is standard php library
spl_autoload_register(function ($class) {
    include 'classes/' . $class . '.php';
});
