<?php

class DB{
    private static $instance = null;
    private $_pdo, 
            $_query, 
            $_error = false, 
            $_results, 
            $_count = 0;
    
    public function __construct() {
        try{
            $this->_pdo = new PDO('mysql:host=localhost;dbname=todo_vezba', 'root', '');
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}

